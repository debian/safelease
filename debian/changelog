safelease (1.0.1-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 1.0.1
  * debian/copyright:
      - Added new rights.
      - Updated upstream copyright years.
  * debian/patches/paths.patch:
      - Added the dep3 tags.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Tue, 16 Jun 2020 22:46:21 +0000

safelease (1.0-3) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Added VCS fields to use Salsa.
      - Added Homepage field.
      - Bumped Standards Version to 4.5.0.
      - Changed the priority from extra to optional.
      - Removed 'dh-autoreconf, autotools-dev' from Build-Depends field.
      - Reorganized file content.
  * debian/copyright:
      - Updated links to use HTTPS protocol.
      - Updated packaging copyright.
  * debian/rules:
      - Added DEB_BUILD_MAINT_OPTIONS variable to improve GCC hardening.
      - Removed '--with autotools-dev,autoreconf'.
  * debian/salsa-ci.yml:
      - Added to provide CI tests for Salsa.
  * debian/watch: added.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Sat, 13 Jun 2020 22:17:06 +0000

safelease (1.0-2) unstable; urgency=medium

  * Orphaned.

 -- Milan Zamazal <pdm@debian.org>  Wed, 11 Jul 2018 14:40:50 +0200

safelease (1.0-1) unstable; urgency=low

  * Initial official Debian package, based on oVirt releng-tools packaging;
    closes: #812329.

 -- Milan Zamazal <mzamazal@redhat.com>  Mon, 25 Jan 2016 13:13:08 +0100
